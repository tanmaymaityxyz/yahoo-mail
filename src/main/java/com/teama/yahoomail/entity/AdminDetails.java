package com.teama.yahoomail.entity;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class AdminDetails implements UserDetails {
	
	
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7100071684967883589L;
	public String adminMail;
	public String password;
	public boolean enabled;
	public List<GrantedAuthority> authorities;
	
	public AdminDetails() {
		
	}
	
	

	public AdminDetails(Admin admin) {
		this.adminMail = admin.getEmail();
		this.password = admin.getPassword();
		this.enabled = admin.isActive();
		this.authorities = Arrays.asList(new SimpleGrantedAuthority(admin.getAuthority()));
	}


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		// TODO Auto-generated method stub
		return this.authorities;
	}

	@Override
	public String getPassword() {
		// TODO Auto-generated method stub
		return this.password;
	}

	@Override
	public String getUsername() {
		// TODO Auto-generated method stub
		return this.adminMail;
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public boolean isEnabled() {
		// TODO Auto-generated method stub
		return this.enabled;
	}

}
