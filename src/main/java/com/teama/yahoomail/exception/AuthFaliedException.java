package com.teama.yahoomail.exception;

public class AuthFaliedException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7571427173438743292L;

	public AuthFaliedException(String message) {
		super(message);
	}
	
}
