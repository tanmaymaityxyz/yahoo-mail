package com.teama.yahoomail.controller;

import java.util.List;
import java.util.Set;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.teama.yahoomail.entity.UserDetail;
import com.teama.yahoomail.exception.AttributeDontExist;
import com.teama.yahoomail.exception.EmailAlreadyUsedException;
import com.teama.yahoomail.exception.EmailFormattingException;
import com.teama.yahoomail.response.UserSucessFullyAdmited;
import com.teama.yahoomail.service.UserDetailService;
import com.teama.yahoomail.utilities.MailUtility;

@RestController
public class UserDetailController {

	@Autowired
	private UserDetailService userDetailService;

	@Autowired
	private MailUtility mailUtility;

	ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
	Validator validator = factory.getValidator();

//	@GetMapping("/users")
//	public List<UserDetail> getAllUsers() {
//		return userDetailService.getAllUsers();
//	}

	@GetMapping("/users")
	public List<UserDetail> getAllUserCustom(@RequestParam("pageno") String pageno,
			@RequestParam("sortby") String sortby,
			@RequestParam("search") String search) {

		int no;
		int size = 5;
		try {
			no = Integer.parseInt(pageno);
		} catch (Exception e) {
			no = 0;
			size = 100;
		}

		String sortByCopy = sortby;
		if (sortby.length() == 0) {
			sortByCopy = "name";
		}

		try {
			Pageable p = PageRequest.of(no, size, Sort.by(sortByCopy).ascending());

			return userDetailService.getAllUsers(search,p);
		} catch (Exception e) {
			throw new AttributeDontExist("Does not exist attribute:" + sortByCopy);
		}
	}

	@PostMapping("/signup")
	public ResponseEntity<UserSucessFullyAdmited> addUser(@RequestBody UserDetail userDetail) {
		String userId;
		findViolation(userDetail);
		if (userDetailService.findByEmail(userDetail.getEmail())) {
			throw new EmailAlreadyUsedException(
					"The email--" + userDetail.getEmail() + "--already exists.use another mail");
		}
		userId = userDetailService.addUser(userDetail);
		 mailUtility.authenticationMail(userId, userDetail.getEmail());
		UserSucessFullyAdmited msg = new UserSucessFullyAdmited(userDetail.getName(), true, HttpStatus.OK.value(),
				userDetail.getEmail());

		return new ResponseEntity<>(msg, HttpStatus.OK);
	}

	private void findViolation(UserDetail userDetail) {
		Set<ConstraintViolation<UserDetail>> violations = validator.validate(userDetail);

		if (violations.size() != 0 || !userDetail.getEmail().contains(".")) {
			throw new EmailFormattingException("Not a valid email ---" + userDetail.getEmail());
		}
	}

}
