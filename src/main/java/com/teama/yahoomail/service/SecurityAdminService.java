package com.teama.yahoomail.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.teama.yahoomail.entity.Admin;
import com.teama.yahoomail.entity.AdminDetails;

@Service
public class SecurityAdminService implements UserDetailsService {
	
	@Autowired
	AdminService adminService;
	
	@Override
	public UserDetails loadUserByUsername(String adminMail) throws UsernameNotFoundException {
		
		Optional<Admin> admin = adminService.getAdmin(adminMail);
		
		if(admin.isPresent()) {
			AdminDetails adminDetail = new AdminDetails(admin.get());
			return adminDetail;
		} else {
			throw new UsernameNotFoundException("You are not an admin");
		}
	}

}
