package com.teama.yahoomail.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.teama.yahoomail.entity.Email;
import com.teama.yahoomail.repository.EmailRepository;
import com.teama.yahoomail.service.EmailService;


@Service
public class EmalServiceImpl implements EmailService {
	
	@Autowired
	EmailRepository emailRepository;
	
	@Override
	public void saveMail(Email email) {
		
		emailRepository.save(email);
	}

}
